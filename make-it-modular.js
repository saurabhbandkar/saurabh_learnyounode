const module1 = require('./module.js');
const dirPath = process.argv[2];
let fileExtension = process.argv[3];

module1(dirPath, fileExtension, (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        data.map((currentFile) => {
            console.log(currentFile);
        });
    };
});