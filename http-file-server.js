const http = require('http');
const portNumber = process.argv[2];
const filePath = process.argv[3];
const fs = require('fs');

const server = http.createServer((request, response) => {
    fs.createReadStream(filePath).pipe(response);
});

server.listen(portNumber);