module.exports = function (dirPath, fileExtensionOld, callback) {
    const fs = require('fs');
    const fileExtension = '.' + fileExtensionOld;

    fs.readdir(dirPath, (err, files) => {
        if (err) {
            callback(err);
        }
        else {
            const result = files.filter((currentFile) => {
                if (currentFile.slice(-fileExtension.length) == fileExtension) {
                    return currentFile
                };
            });
            callback(null, result);
        };
    });
};

