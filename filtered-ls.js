const fs = require('fs');
const dirPath = process.argv[2];
const fileExtension = '.' + process.argv[3];

fs.readdir(dirPath, (err, files) => {
    if (err) {
        console.log(err);
    }
    else {
        const result = files.map((currentFile) => {
            if (currentFile.slice(-fileExtension.length) == fileExtension) {
                console.log(currentFile);
                return currentFile
            };
        });
    };
});