const http = require('http');
const portNumber = process.argv[2];
const url = require('url');

const routes = {
    "/api/parsetime": function (parsedUrl) {
        const date = new Date(parsedUrl.query.iso);
        return {
            hour: date.getHours(),
            minute: date.getMinutes(),
            second: date.getSeconds()
        };
    },
    "/api/unixtime": function (parsedUrl) {
        return { unixtime: (new Date(parsedUrl.query.iso)).getTime() };
    }
};
const server = http.createServer((request, response) => {
    parsedUrl = url.parse(request.url, true);
    resource = routes[parsedUrl.pathname];
    if (resource) {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.end(JSON.stringify(resource(parsedUrl)));
    }
    else {
        response.writeHead(404);
        response.end();
    };
});

server.listen(portNumber);