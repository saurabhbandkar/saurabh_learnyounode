const fs = require('fs');
const filePath = process.argv[2];

fs.readFile (filePath, 'utf-8', (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        const fileData = data.toString();
        const lines = fileData.split('\n');
        console.log (lines.length -1);
    };
});