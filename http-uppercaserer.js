const http = require('http');
const portNumber = process.argv[2];
const map = require('through2-map');

const server = http.createServer((request, response) => {
    const upperCase = map(function (data) {
        return data.toString().toUpperCase();
    });
    if (request.method == 'POST') {
        request.pipe(upperCase).pipe(response);
    };
});

server.listen(portNumber);