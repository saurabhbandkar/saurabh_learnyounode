const http = require('http');
const url = process.argv[2];
let result = '';

http.get(url, (response) => {

    response.setEncoding('utf8');
    response
        .on('data', (data) => {
            result = result + data.toString();
        })
        .on('end', () => {
            console.log (result.split('').length)
            console.log(result);
        });
}).on('error', (err) => {
    console.error(err);
});