const net = require('net');
const portNumber = process.argv[2];

const server = net.createServer((socket) => {
    const getTime = () => {
        function addZero(anything) {
            if (anything.toString().length == 1) {
                anything = '0' + anything.toString();
                return anything;
            }
            else {
                return anything.toString();
            };
        };
        const date = new Date();
        const year = date.getFullYear().toString();
        const month = addZero(1 + parseInt(date.getMonth()));
        const day = addZero(date.getDate());
        const hours = addZero(date.getHours());
        const minutes = addZero(date.getMinutes());
        const time = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes;
        return time;
    };
    const time = getTime();
    socket.end(time + '\n');
});



server.listen(portNumber);

