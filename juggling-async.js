const http = require('http');
const urlList = [process.argv[2], process.argv[3], process.argv[4]];

function getData(url, callback) {
    let result = '';
    http.get(url, (response) => {
        response.setEncoding('utf8');
        response
            .on('data', (data) => {
                result = result + data.toString();
            })
            .on('end', () => {
                callback(null, result);
            });
    }).on('error', (err) => {
        callback(err, null);
    });
};

getData(urlList[0], (err, data) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
        getData(urlList[1], (err, data) => {
            if (err) {
                console.error(err);
            }
            else {
                console.log(data);
                getData(urlList[2], (err, data) => {
                    if (err) {
                        console.error(err);
                    }
                    else {
                        console.log(data);
                    };
                });
            };
        });
    };
});







