const http = require('http');
const url = process.argv[2];

http.get(url, (response) => {

    response.setEncoding('utf8');
    let rawData = '';
    response.on('data', (rawData) => {
        console.log(rawData);
    });
    response.on('end', () => {
        console.log('');
    });
}).on('error', (err) => {
    console.error(`Got error: ${err.message}`);
});